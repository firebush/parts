***************
Release  0.17.7
***************

* update InstallCMakeConfig, SdkCMakeConfig and logic to the ScanDirectory default logic to find the cmake config files.
* Added logic for a node to be tagged with SKIP_RPATH to avoid that file from being processed by the rpath logic used in packaging.
